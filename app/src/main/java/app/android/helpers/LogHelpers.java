package app.android.helpers;

import android.util.Log;

/**
 * Created by Aine on 03/12/2015.
 */
public class LogHelpers
{
    public static void info(Object parent, String message)
    {
        Log.i(parent.getClass().getSimpleName(), message);
    }
}
